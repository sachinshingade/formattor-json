import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formattor',
  templateUrl: './formattor.component.html',
  styleUrls: ['./formattor.component.scss']
})
export class FormattorComponent implements OnInit {

  rawJson: any;
  formatedJson: any;
  constructor() { }

  ngOnInit() {
    // this.formatJson();
  }
  formatJson() {
    let rawJSONObject
    try {
      if (this.rawJson == '' || this.rawJson == undefined || this.rawJson == null) {
        alert("Enter a raw JSON");
        return
      }
      rawJSONObject = JSON.parse(this.rawJson)
    } catch(e){
      console.error("error",e);
      alert(e);
      return
    }
    console.log('rw', rawJSONObject);
    console.log('rw1', typeof  rawJSONObject);
    let result = [];
    if (typeof rawJSONObject == 'object') {
      console.log('true', rawJSONObject);
      for (const key in rawJSONObject) {
        if (key == '0') {
          console.log('0', rawJSONObject[key]);
          rawJSONObject[key].map((ob) => {
            console.log("OBJ",ob)
            result.push(ob);
          });
        } else {
          rawJSONObject[key].map((ob)=>{
            console.log(key, ob);
            // result.map((levelOne)=>{
            //   if(levelOne.id == ob.parent_id){
            //     levelOne.children.push(ob);
            //   }
            //   if(levelOne.children){
            //     levelOne.children.map((levelTwo)=>{
            //       if(levelTwo.id == ob.parent_id){
            //         levelTwo.children.push(ob);
            //       }
            //     })
            //   }
            // })
            result = this.repetative(result, ob)
          })
        }
      }
      // console.log('rr', result);
      this.formatedJson = JSON.stringify(result, null, 2);
    }
  }

  repetative(res, ob) {
    res.map((item) => {
      if(item.id == ob.parent_id){
        item.children.push(ob);
      }
      if(item.children) {
        this.repetative(item.children, ob)
      }
    })
    return res;
  }

  clearAll() {
    this.rawJson = "";
    this.formatedJson = "";
  }

  // get rapidPageValue() {
  //   return JSON.stringify(this.rawJson, null, 2);
  // }

  // set rapidPageValue(v) {
  //   console.log("t",v)
  //   try {
  //     this.rawJson = JSON.parse(v);
  //   } catch (e) {
  //     console.log('error occored while you were typing the JSON');
  //   }
  // }
}
