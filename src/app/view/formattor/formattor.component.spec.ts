import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormattorComponent } from './formattor.component';

describe('FormattorComponent', () => {
  let component: FormattorComponent;
  let fixture: ComponentFixture<FormattorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormattorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormattorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
