import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormattorComponent } from './view/formattor/formattor.component'


const routes: Routes = [
  {
    path: "",
    "redirectTo" : "/format",
    "pathMatch" :'full'
  },
  {
    "path" : "format",
    "component" : FormattorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
